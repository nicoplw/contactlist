package com.solstice_mobile.hireme.contactlist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.solstice_mobile.hireme.contactlist.models.Contact;
import org.json.JSONArray;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    ObjectMapper mapper = new ObjectMapper();
    ListView listView;
    ArrayList<Contact> results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listView = (ListView) findViewById(R.id.listView);
        String url = getResources().getString(R.string.json_url);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(GetJsonArray(url));

        listViewClickListener();
    }

    @NonNull
    private JsonArrayRequest GetJsonArray(String url) {
        return new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    if (response.length() > 0) {
                        results = mapper.readValue(response.toString(),
                                new TypeReference<ArrayList<Contact>>() { } );

                        ContactsAdapter adapter = new ContactsAdapter(MainActivity.this, R.layout.list_view_items, results);
                        listView.setAdapter(adapter);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void listViewClickListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Contact contact = results.get(position);
                long timestamp = Long.parseLong(contact.getBirthdate()) * 1000;

                Intent intent = new Intent(MainActivity.this,DetailsActivity.class);
                Bundle b = new Bundle();
                b.putString("contactName", contact.getName());
                b.putString("companyName", contact.getCompany());
                b.putString("homePhone", contact.getPhone().getHome());
                b.putString("workPhone", contact.getPhone().getWork());
                b.putString("mobilePhone", contact.getPhone().getMobile());
                b.putString("detailsUrl", contact.getDetailsURL());
                b.putString("birthday", getDate(timestamp));

                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    private String getDate(long timeStamp){

        try{
            DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

}
