package com.solstice_mobile.hireme.contactlist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.solstice_mobile.hireme.contactlist.models.Address;
import com.solstice_mobile.hireme.contactlist.models.Details;
import com.squareup.picasso.Picasso;
import org.json.JSONObject;
import java.io.IOException;

public class DetailsActivity extends AppCompatActivity {

    ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.details));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle b = getIntent().getExtras();
        fillItems(b);
        String url = b.getString("detailsUrl");
        JsonObjectRequest jsObjRequest = getJsonObjectRequest(url);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsObjRequest);
    }

    @NonNull
    private JsonObjectRequest getJsonObjectRequest(String url) {
        return new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Details result = mapper.readValue(String.valueOf(response), Details.class);
                                TextView address1 = (TextView) findViewById(R.id.address1TextView);
                                TextView address2 = (TextView) findViewById(R.id.address2TextView);
                                TextView email = (TextView) findViewById(R.id.emailTextView);
                                ImageView largeImage = (ImageView) findViewById(R.id.largeImageView);
                                ImageView starImage = (ImageView) findViewById(R.id.starImageView);
                                Address address = result.getAddress();
                                address1.setText(address.getStreet());
                                String location = address.getCity();
                                location = location + ", " + address.getState();
                                address2.setText(location);
                                email.setText(result.getEmail());
                                Picasso.with(getApplicationContext())
                                        .load(result.getLargeImageURL())
                                        .into(largeImage);
                                if (result.getFavorite()){
                                    starImage.setVisibility(View.GONE);
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            String a = response.toString();


                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_internet), Toast.LENGTH_LONG).show();
                        }
                    });
    }

    private void fillItems(Bundle b){
        TextView contactName = (TextView) findViewById(R.id.contactNameTextView);
        TextView companyName = (TextView) findViewById(R.id.companyTextView);
        TextView homePhone = (TextView) findViewById(R.id.homePhoneTextView);
        TextView workPhone = (TextView) findViewById(R.id.workPhoneTextView);
        TextView mobilePhone = (TextView) findViewById(R.id.mobilePhoneTextView);
        TextView birthday = (TextView) findViewById(R.id.birthdayTextView);
        TableRow homeRow = (TableRow) findViewById(R.id.row1);
        TableRow workRow = (TableRow) findViewById(R.id.row2);
        TableRow mobileRow = (TableRow) findViewById(R.id.row3);


        contactName.setText(b.getString("contactName"));
        companyName.setText(b.getString("companyName"));
        homePhone.setText(b.getString("homePhone"));
        workPhone.setText(b.getString("workPhone"));
        mobilePhone.setText(b.getString("mobilePhone"));
        birthday.setText(b.getString("birthday"));

        phoneHider(homePhone, homeRow);
        phoneHider(workPhone, workRow);
        phoneHider(mobilePhone, mobileRow);
    }

    private void phoneHider(TextView phone, TableRow row) {
        if(phone.getText().toString().isEmpty()){
            row.setVisibility(View.GONE);
        }
    }
}
