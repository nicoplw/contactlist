package com.solstice_mobile.hireme.contactlist.models;

public class Phone {
    private String work;
    private String home;
    private String mobile;

    public String getWork() {
        return work;
    }
    public String getHome() {
        return home;
    }
    public String getMobile() {
        return mobile;
    }

    public void setWork(String sWork) {
        work = sWork;
    }
    public void setHome(String sHome) {
        home = sHome;
    }
    public void setMobile(String sMobile) {
        mobile = sMobile;
    }
}