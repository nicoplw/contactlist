package com.solstice_mobile.hireme.contactlist.models;

public class Contact {
    private String name;
    private int employeeId;
    private String company;
    private String detailsURL;
    private String smallImageURL;
    private String birthdate;
    private Phone phone;

    public String getName() {
        return name;
    }
    public int getEmployeeId() {
        return employeeId;
    }
    public String getCompany() {
        return company;
    }
    public String getDetailsURL() {
        return detailsURL;
    }
    public String getSmallImageURL() {
        return smallImageURL;
    }
    public String getBirthdate() {
        return birthdate;
    }
    public Phone getPhone() {
        return phone;
    }

    public void setName(String sName) {
        name = sName;
    }
    public void setEmployeeId(int sEmployeeId) {
        employeeId = sEmployeeId;
    }
    public void setCompany(String sCompany) {
        company = sCompany;
    }
    public void setDetailsURL(String sDetailsUrl) {
        detailsURL = sDetailsUrl;
    }
    public void setSmallImageURL(String sSmallImageURL) {
        smallImageURL = sSmallImageURL;
    }
    public void setBirthdate(String sBirthdate) {
        birthdate = sBirthdate;
    }
    public void setPhone(Phone sPhone) {
        phone = sPhone;
    }
}