package com.solstice_mobile.hireme.contactlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.solstice_mobile.hireme.contactlist.models.Contact;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ContactsAdapter extends ArrayAdapter<Contact> {

    ArrayList<Contact> contactList = new ArrayList<>();

    public ContactsAdapter(Context context, int textViewResourceId, ArrayList<Contact> objects) {
        super(context, textViewResourceId, objects);
        contactList = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.list_view_items, null);
        TextView contactTextView = (TextView) v.findViewById(R.id.contactNameTextView);
        TextView phoneTextView = (TextView) v.findViewById(R.id.phoneTextView);
        ImageView imageView = (ImageView) v.findViewById(R.id.smallImageView);
        contactTextView.setText(contactList.get(position).getName());
        phoneTextView.setText(contactList.get(position).getPhone().getHome());
        Picasso.with(parent.getContext())
                .load(contactList.get(position).getSmallImageURL())
                .into(imageView);
        return v;
    }

}
