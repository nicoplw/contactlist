package com.solstice_mobile.hireme.contactlist.models;

public class Details {
    private int employeeId;
    private boolean favorite;
    private String largeImageURL;
    private String email;
    private String website;
    private Address address;

    public int getEmployeeId() { return employeeId; }
    public boolean getFavorite() {
        return favorite;
    }
    public String getLargeImageURL() {
        return largeImageURL;
    }
    public String getEmail() {
        return email;
    }
    public String getWebsite() {
        return website;
    }
    public Address getAddress() {
        return address;
    }

    public void setEmployeeId(int sEmployeeId) { employeeId = sEmployeeId; }
    public void setFavorite(boolean sFavorite) { favorite = sFavorite; }
    public void setLargeImageURL(String sLargeImageURL) {
        largeImageURL = sLargeImageURL;
    }
    public void setEmail(String sEmail) { email = sEmail; }
    public void setWebsite(String sWebsite) { website = sWebsite; }
    public void setAddress(Address sAddress) { address = sAddress; }
}