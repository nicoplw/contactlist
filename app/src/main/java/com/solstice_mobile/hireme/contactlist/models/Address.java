package com.solstice_mobile.hireme.contactlist.models;

public class Address {
    private String street;
    private String city;
    private String state;
    private String country;
    private String zip;
    private double latitude;
    private double longitude;

    public String getStreet() {
        return street;
    }
    public String getCity() {
        return city;
    }
    public String getState() {
        return state;
    }
    public String getCountry() {
        return country;
    }
    public String getZip() {
        return zip;
    }
    public double getLatitude() {
        return latitude;
    }
    public double getLongitude() {
        return longitude;
    }

    public void setStreet(String sStreet) { street = sStreet; }
    public void setCity(String sCity) { city = sCity; }
    public void setState(String sState) { state = sState; }
    public void setCountry(String sCountry) { country = sCountry; }
    public void setZip(String sZip) { zip = sZip; }
    public void setLatitude(double sLatitude) { latitude = sLatitude; }
    public void setLongitude(double sLongitude) { longitude = sLongitude; }

}